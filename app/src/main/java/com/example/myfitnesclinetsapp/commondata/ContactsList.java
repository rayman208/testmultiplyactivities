package com.example.myfitnesclinetsapp.commondata;

import java.util.ArrayList;

public class ContactsList
{
    private ArrayList<Contact> contacts;

    public ContactsList()
    {
        contacts = new ArrayList<>();
    }

    public void Add(Contact contact)
    {
        contacts.add(contact);
    }

    public ArrayList<String> GetContactsInStrings()
    {
        ArrayList<String> strings = new ArrayList<>();

        for (int i = 0; i < contacts.size(); i++)
        {
            String output = "Name: "+contacts.get(i).Name+"\n"+
                    "Birthday: "+contacts.get(i).Birthday+"\n"+
                    "Weight: "+contacts.get(i).Weight;

            strings.add(output);
        }

        return strings;
    }
}
