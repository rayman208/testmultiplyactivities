package com.example.myfitnesclinetsapp.commondata;

import java.util.Date;

public class Contact
{
    public String Name;
    public Date Birthday;
    public int Weight;

    public Contact(String Name,Date Birthday,int Weight)
    {
        this.Name = Name;
        this.Birthday = Birthday;
        this.Weight = Weight;
    }
}
