package com.example.myfitnesclinetsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.SeekBar;

import com.example.myfitnesclinetsapp.commondata.Contact;
import com.example.myfitnesclinetsapp.commondata.DataTransferClass;

import java.util.Date;

public class ContactAddActivity extends AppCompatActivity {

    EditText editTextName;
    CalendarView calendarViewBirthDay;
    SeekBar seekBarWeight;
    Button buttonAddClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_add);

        editTextName = findViewById(R.id.editTextName);
        calendarViewBirthDay = findViewById(R.id.calendarViewBirthDay);
        seekBarWeight = findViewById(R.id.seekBarWeight);

        buttonAddClose = findViewById(R.id.buttonAddClose);
        buttonAddClose.setOnClickListener(buttonAddCloseOnClick);
    }

    View.OnClickListener buttonAddCloseOnClick = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            String name = editTextName.getText().toString();
            Date birthday = new Date(calendarViewBirthDay.getDate());
            int weight = seekBarWeight.getProgress();

            DataTransferClass.ContactsList.Add(new Contact(name, birthday, weight));

            finish();
        }
    };
}
