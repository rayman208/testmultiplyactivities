package com.example.myfitnesclinetsapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.myfitnesclinetsapp.commondata.DataTransferClass;

import java.net.Inet4Address;

public class MainActivity extends AppCompatActivity {

    Button buttonAddContact;
    ListView listViewContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonAddContact = findViewById(R.id.buttonAddContact);
        listViewContacts = findViewById(R.id.listViewContacts);

        buttonAddContact.setOnClickListener(buttonAddContactOnClick);
    }

    View.OnClickListener buttonAddContactOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            Intent intent = new Intent(getApplicationContext(), ContactAddActivity.class);
            startActivityForResult(intent,1);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_list_item_1, DataTransferClass.ContactsList.GetContactsInStrings());

        listViewContacts.setAdapter(adapter);
    }
}
